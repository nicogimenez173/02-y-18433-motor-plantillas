const express = require("express");
const db = require("./db/data");
const hbs = require ("hbs");
const app = express();



app.use(express.static(__dirname + "/public"));
app.set('view engine', 'hbs');
app.set('views', __dirname + "/views");

hbs.registerPartials(__dirname + "/views/partials");

app.get("/", (req, res) => {
    res.render("index",{integrantes: db.integrantes,});
});

const matriculas = [...new Set(db.media2.map(item => item.matricula))];
app.get("/:matricula", (request, response) => {
    const matricula = request.params.matricula;
    console.log(matricula)
    if (matriculas.includes(matricula)) {
        const media2Filtrada = db.media2.filter(item => item.matricula === matricula);
        const integrantesFiltrados = db.integrantes.filter(item => item.matricula === matricula);
        response.render('conjunto', {
            media: db.media,
            integrantes: integrantesFiltrados,
            media2: media2Filtrada,
        });
    }else{
      app.use((req, res, next) => {
        res.status(404).render('partials/error');
    });  
    }
    
});














app.listen(3000, () => {
    console.log("Servidor coriendo en el puerto 3000");
    console.log("http://localhost:3000/");
});
    
console.log ("base de datos simulada", db);
console.log (db.integrantes);